import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HeaderService } from '../../services/header.service';
import { SearchService } from 'src/app/services/search.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  public logoImg : string = "../../assets/png/logo_text.png";
  public logoFiremap: string="../../assets/png/logo_text.png";
  public searchbarContent: string = '';
  public displayNavigation: boolean;
  public navigationId: number;
  
  constructor(
    private router: Router,
    private _headerService: HeaderService,
    private _searchService: SearchService
    ) { }

  ngOnInit() {
    this.displayNavigation = this._headerService.getNavigationState();
    this.navigationId = this._headerService.getNavigationId();
  }

  searchbarSubmit() {

    setTimeout(() => { 
      this._searchService.setQuery(this.searchbarContent); 
      this.router.navigate(['/search'], { queryParams: { query: this.searchbarContent } });
    }, 200);
  }


  handlePlanClick() {

    this.router.navigate(['/plan'], { queryParams: { query: this.navigationId } });
  }

  handleDetailClick() {

    this.router.navigate(['/detail'], { queryParams: { query: this.navigationId } });
  }

  handleLogoClick() {

    this.router.navigate(['/home']);
  }

}
