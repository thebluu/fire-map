import { Component, OnInit } from '@angular/core';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { Plan } from 'src/app/models/plan';
import { PlanService } from 'src/app/services/plan.service';
import { HeaderService } from "../../services/header.service";

@Component({
  selector: 'app-plan',
  templateUrl: './plan.component.html',
  styleUrls: ['./plan.component.scss']
})
export class PlanComponent implements OnInit {

  public pdfSrc: string;
  public plan: Plan;

  constructor(
    private _headerService: HeaderService,
    private _planService: PlanService) { }

  ngOnInit() {

    this._headerService.setNavigationState(true);

    this._planService.getId_mock(1)
      .subscribe(
        (data) => this.handlePlan(data)
      );
  }

  handlePlan(data: any) {

    this.plan = data;
    this.pdfSrc = '/assets/mock/plan.pdf';
  }
}