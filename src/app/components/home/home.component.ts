import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { LocalStorage } from '@ngx-pwa/local-storage';
import { SearchService } from "../../services/search.service";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  public logoFiremap: string="../../assets/png/logo_text.png";
  public searchbarContent: string = '';
  public localPlans = []

  constructor(
    private router: Router,
    private _localStorage: LocalStorage,
    private _searchService: SearchService,) {

      this.routeEvent();  
    }

  ngOnInit() {

  }

  routeEvent() {

    this.router.events.subscribe(e => {

      if(e instanceof NavigationEnd){
        if(e.urlAfterRedirects.includes('/home')) {
          
          this._localStorage.getItem('plans').subscribe((plans) => {
            this.localPlans = plans;
            console.log(this.localPlans);
          });
        }
      }
    });
  }

  hasLocalPlans() {
    
    if(this.localPlans) {
      return this.localPlans.length>0;
    }
    else {
      return (this.localPlans);
    }
  }

  
  searchbarSubmit() {

    setTimeout(() => { 
      this._searchService.setQuery(this.searchbarContent); 
      this.router.navigate(['/search']);
    }, 200);
    
  }

  handlePlanClick(id: number) {

    this.router.navigate(['/detail']);
  }

  searchbarInput() {
  }
}
