import { Component, OnInit } from '@angular/core';
import { PlanService} from '../../services/plan.service';
import { Plan } from 'src/app/models/plan';
import { Detail } from 'src/app/models/detail';
import { HeaderService } from "../../services/header.service";
import { LocalStorage } from '@ngx-pwa/local-storage';
import { Router, NavigationStart, NavigationEnd } from '@angular/router';


@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {

  public plan: Plan;
  public descriptif : Detail;
  public prevention : Detail;
  public energie : Detail;
  public locaux : Detail;
  public risque : Detail;
  public contact : Detail;

  constructor(
    private _planService: PlanService,
    private _headerService: HeaderService,
    private _localStorage: LocalStorage,
    private router: Router,) {

      this.routeEvent();
    }

  ngOnInit() {
    
    this._headerService.setNavigationState(true);

  }

routeEvent(): any {

  this.router.events.subscribe(e => {

    if (e instanceof NavigationStart && e.url.includes('/detail')) {
      this._headerService.setNavigationState(true);
    }

    if (e instanceof NavigationEnd && e.url.includes('/detail')) {
      this._planService.getId_mock(1)
        .subscribe(
          (data) => {
            this.handlePlanResult(data);
            this.storeInLocal();
          });
    }
  });
}

  storeInLocal() {

      this._localStorage.getItem<Plan[]>('plans').subscribe((local) => {

        if(local) {
          let index = local.findIndex((item) => item.id == this.plan.id);
          if (index === -1) {
            local.push(this.plan)
          } else {
            local[index] = this.plan;
          }
        } else {
          local = Array(this.plan);
        }

        this._localStorage.setItem('plans', local).subscribe((test) => {});
      });
  }

  handlePlanResult(data: Plan){

    this.plan = data;
    data.details.forEach((detail) => {
      switch (detail.titre) {
        case 'Descriptif':
          this.descriptif = detail;
          break;
        case 'Moyen de prévention / protection':
          this.prevention = detail;
        case 'Energie':
          this.energie = detail;
        case 'Locaux à protéger en priorité':
          this.locaux = detail;
        case 'Risque principaux':
          this.risque = detail;
        case 'Contact utiles':
          this.contact = detail;
        default:
          break;
      }
    });
  }

  splitmort(s) {
    return s.split(' ; ');
  }
}
