import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, NavigationStart, Router } from '@angular/router';
import * as mapboxgl from 'mapbox-gl';
import { SearchService } from 'src/app/services/search.service';
import { FeatureCollection } from '../../models/geo-json';
import { HeaderService } from "../../services/header.service";
import { Observable, Subscription } from 'rxjs';


@Component({
  selector: 'app-search-result',
  templateUrl: './search-result.component.html',
  styleUrls: ['search-result.component.scss']
})
export class SearchResultComponent implements OnInit {

  public results = [];
  public map: any;
  public featureCollection: FeatureCollection;
  public query: string;
  public searchIsPending: boolean;

  constructor(
    private _searchService: SearchService,
    private router: Router,
    private _headerService: HeaderService) {

      this.routeEvent(this.router);
    }

  ngOnInit() {
  }

  routeEvent(router: Router): any {

    router.events.subscribe(e => {

      if(e instanceof NavigationStart){
        if(e.url.includes('/search')) {

          this.cleanProperties();
        }
      }

      if(e instanceof NavigationEnd){
        if(e.url.includes('/search')) {
          
          this.search();
        }
      }
    });
  }

  handleSearchResult(data: any) {

    this.searchIsPending = false;
    this.results = data;
    if (this.hasResults()) {
      mapboxgl.accessToken = 'pk.eyJ1IjoiYmx1ZWJlYW4iLCJhIjoiY2pzdDIxdXVmMWp5MDN5b2VuYmcwYzk1bCJ9.X3aMtlHcRlR9f66g8DitKA';
      this.initMap();
      this.addMarkers();
    }
  }

  handleSearchError(error) {
    
    this.searchIsPending = false;
    this.router.navigate(['/404']);    
  } 

  initMap() {

    this.map = new mapboxgl.Map({
      container: 'map', // container id
      style: 'mapbox://styles/mapbox/outdoors-v9', //stylesheet location
      center: [-1.55, 47.21], // starting position
      zoom: 11, // starting zoom
    });
  }

  addMarkers() {
    this.featureCollection = new FeatureCollection(this.results.map(plan => plan.geojson));
    this.featureCollection.features.forEach((marker) => {
      let el = document.createElement('div');
      el.style.backgroundImage = 'url(/assets/png/marker.png)';              
      el.style.width = '40px';                                                      
      el.style.height = '40px';
      el.style.borderRadius = '50%'; 
      el.style.backgroundSize = 'contain';
      el.className = 'marker';
      new mapboxgl.Marker(el)
        .setLngLat(marker.geometry.coordinates)
        .addTo(this.map);
    });
  }

  hasResults() {

    return this.results.length > 0;
  }

  cleanProperties() {

    this.map = null;
    this.featureCollection = null;
    this.results = [];
  }

  search() {

    if (this._searchService.getQuery()) {
      this._searchService.list_mock()
        .subscribe(
          (data) => this.handleSearchResult(data),
          (error) => this.handleSearchError(error));
      this.searchIsPending = true;
    }
  }

  handleResultClick(id: number) {
        
    this._headerService.setNavigationId(id);

    this.router.navigate(['/detail'], { queryParams: { query: id } });
  }

  skeletonArray() {

    return [1, 2, 3];
  }

  ngAfterViewChecked() {

    if (this.map) this.map.resize();
  }

}