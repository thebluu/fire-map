import { Adresse, AdresseAdapter } from "./adresse";
import { Detail, DetailAdapter } from "./detail";
import { Injectable } from "@angular/core";
import { Adapter } from "../core/adapter";

export class Plan {
    constructor(
    public id: number,
    public nom_batiment: string,
    public date_creation : Date,
    public fichier : string,
    public extension_fichier : string,
    public numero_etage : string,
    public type_plan : string,
    public annotation : string,
    public details : Detail [], 
    public adresse : Adresse,
    ){}
}

@Injectable({
    providedIn: 'root'
})

    export class PlanAdapter implements Adapter<Plan>{

        constructor( 
            private detailAdapter: DetailAdapter, 
            private adresseAdapter: AdresseAdapter){}

        adapt(item: any): Plan {
            
            return new Plan(
                item.id,
                item.nom_batiment,
                item.date_creation,
                item.fichier,
                item.extension_fichier,
                item.numero_etage,
                item.type_plan,
                item.annotation,
                item.details.map(detail=>this.detailAdapter.adapt(detail)),
                this.adresseAdapter.adapt(item.adresse),
            );
        }
    }