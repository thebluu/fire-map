import { Injectable } from "@angular/core";
import { Adapter } from "../core/adapter";

export class Adresse {
    constructor(
        public numero_rue : string,
        public nom_rue : string,
        public code_postal : string,
        public ville : string,
        public pays : string,
        public latitude : string,
        public longitude : string,
    ){}
}

@Injectable({
    providedIn: 'root'
})

    export class AdresseAdapter implements Adapter<Adresse>{

        constructor(){}

        adapt(item: any): Adresse {
            return new Adresse(
                item.numero_rue,
                item.nom_rue,
                item.code_postal,
                item.ville,
                item.pays,
                item.latitude,
                item.longitude,
            );
        }
    }
