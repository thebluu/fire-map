import { Injectable } from "@angular/core";
import { Adapter } from "../core/adapter";

export class Detail {
    constructor(
        public titre : string,
        public description : string,
    ){}
}

@Injectable({
    providedIn: 'root'
})

    export class DetailAdapter implements Adapter<Detail>{

        constructor(){}

        adapt(item: any): Detail {
            return new Detail(
                item.titre,
                item.description,
            );
        }
    }
