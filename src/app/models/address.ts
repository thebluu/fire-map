import { Injectable } from '@angular/core';
import { Adapter } from '../core/adapter';
import { GeoJson, GeoJsonAdapter } from './geo-json';

export class Address {
    constructor(
        public id: string,
        public title: string,
        public address: string,
        public image_url: string,
        public geojson: GeoJson,
    ) { }
}

@Injectable({
    providedIn: 'root'
})
  export class SearchAdapter implements Adapter<Address> {
    
    constructor(private geoJsonAdapter: GeoJsonAdapter){}

    adapt(item: any): Address {
        return new Address(
            item.id,
            item.title,
            item.address,
            item.image_url,
            this.geoJsonAdapter.adapt([Number(item.coordinates.split(',')[0]), Number(item.coordinates.split(',')[1])]),
        );
    }
  }