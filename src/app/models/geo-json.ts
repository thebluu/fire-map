import { Injectable } from "@angular/core";
import { Adapter } from "../core/adapter";

export interface IGeometry {
    type: string;
    coordinates: number[];
}

export interface IGeoJson {
    type: string;
    geometry: IGeometry;
    properties?: any;
    $key?: string;
}

export class GeoJson implements IGeoJson {
  type = 'Feature';
  geometry: IGeometry;

  constructor(coordinates, public properties?) {
    this.geometry = {
      type: 'Point',
      coordinates: coordinates
    }
  }
}

export class FeatureCollection {
  type = 'FeatureCollection'
  constructor(public features: Array<GeoJson>) {}
}

@Injectable({
  providedIn: 'root'
})
  export class GeoJsonAdapter implements Adapter<GeoJson> {

    adapt(item: any): GeoJson {
        let coordinates: number[] = item.map(parseFloat);
        return new GeoJson(coordinates);
    }
  }