import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs'
import { Address, SearchAdapter } from '../models/address';
import { map } from "rxjs/operators";
import { of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  public url: string = 'https://firemap-backend.herokuapp.com/adresses/?search=';
  public query: string
  private mock_plans: any[] = [
    {
      id: 'id1',
      title: 'Hangar',
      address: '76 Boulevard de l\'Estuaire',
      image_url: 'http://placehold.it/75',
      coordinates: '-1.571745, 47.220007',
    },

    {
      id: 'id2',
      title: 'Immeuble',
      address: '9 Rue Guibal',
      image_url: 'http://placehold.it/75',
      coordinates: '-1.556311, 47.203915',
    },

    {
      id: 'id3',
      title: 'Maison',
      address: '17 Rue de la Martinière',
      image_url: 'http://placehold.it/75',
      coordinates: '-1.543418, 47.225941,',
    },

    {
      id: 'id4',
      title: 'Immeuble',
      address: '8 Rue Général Buat',
      image_url: 'http://placehold.it/75',
      coordinates: '-1.571222, 47.233879',
    },
  ]

  constructor(
    private http: HttpClient,
    private searchAdapter: SearchAdapter,
  ) { }

  list_mock(): Observable<Address[]> {
    return of(this.mock_plans).pipe(
      map((data: any[]) => data.map(item=>this.searchAdapter.adapt(item)))
    );
  }

  /**
   * get list of plans from api
   */
  // list(): Observable<Address[]> {
  //   console.log(this.url+this.query);
    
  //   return this.http.get<Address[]>(this.url+this.query).pipe(
  //     map((data: any) => data.results.map(item=>this.searchAdapter.adapt(item)))
  //   );
  // }

  setQuery(query: string) {

    this.query = query;
  }

  getQuery(): string {
    
    return this.query;
  }
}

