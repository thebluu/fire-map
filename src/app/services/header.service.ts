import { Injectable, SimpleChanges } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class HeaderService {

  public displayNavigation: boolean = false;
  public navigationId: number;
  
  constructor(
  ) { }

  getNavigationState(): boolean {

    return this.displayNavigation;
  }

  setNavigationState(displayNavigation: boolean) {

    this.displayNavigation = displayNavigation;
  }

  getNavigationId(): number {

    return this.navigationId;
  }

  setNavigationId(id: number) {

    this.navigationId = id;
  }

}
