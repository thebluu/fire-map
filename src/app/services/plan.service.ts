import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PlanAdapter, Plan } from '../models/plan';
import { Observable } from 'rxjs'
import { map } from "rxjs/operators";
import { of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PlanService {

  public _url:string = 'http://firemap-backend.herokuapp.com/plans/'

  public mock_plan: any = {
    id: '1',
    nom_batiment: 'immeuble test',
    date_creation : '2019-07-03 hh:mm:ss',
    fichier : 'planFichier',
    extension_fichier : 'pdf',
    numero_etage : '0',
    type_plan : 'type_plan',
    annotation : 'annotation',
    details : [
       { 
        titre : 'Descriptif',
        description : 'Immeuble ERP de type M (Magasin) d\'avant 1900 et 1200m2 par niveaux totalisant 6 niveaux  ; '+
        'Immeuble non recoupé R+6 / -1 ; ' + 
        'Structure Pierre, Charpente béton, toiture béton ; '+
        'Effectif maxi de jour : 2200 personnes ; Effectif maxi de nuit : 0 ; '+
        'Présence d\'un service de sécurité lors de l\'ouverture au public ; '+
        'Pas de gardiennage la nuit.',
      },
      { 
        titre : 'Moyen de prévention / protection',
        description : 'SSI A localisé au PC sécurité au niveau 0 ; '+
        'Extinction automatique à eau généralisé à l\'immeuble ;'+ 
        'Désenfumage mécanique ; '+
        'colonne sèche aliementant les étages, alimentation par rue du Calvaire ; '+
        'Robinets d\'incendie armés.',
      },
      { 
        titre : 'Energie',
        description : 'Adduction en électricité par câble souterrain et local Haute tension ; '+
        'Adduction en gaz ; '+
        'Réserve en fuel au 6ème étage pour alimentation groupe électrogène'+
        'Présence d\'un groupe électrogène reprenant l\'alimentation générale de l\'immeuble.',
      },
      { 
        titre : 'Locaux à protéger en priorité',
        description : 'Bureaux au sous-sol et administration Kiabi au 3ème étage',
      },
      { 
        titre : 'Risque principaux',
        description : 'Public vulnérable, accueil de PMR ; '+ 
        'Locaux à risques : chaufferie gaz / locaux HT et TGBT / Groupe électrogène / Locaux sprinklage / local ondulateur / machinerie ascenseur / local vide ordures avec compresseur de cartons.',
      },
      { 
        titre : 'Contact utiles',
        description : 'Mr XXXX : Responsable unique de sécurité au 06 00 00 00 00 ; ' + 
        'Mr CofelyAxima : astreinte technique au 06 00 00 00 00',
      },
      
    ],
    adresse : {
      numero_rue : '4',
      nom_rue : 'villa maria',
      code_postal : '44000',
      ville : 'Nantes',
      pays : 'France',
      latitude : '47.230430',
      longitude : '-1.562750',
    },
  }

  constructor(
    private http: HttpClient,
    private planAdapter : PlanAdapter
  ){}

  getId_mock(id: number): Observable<Plan> {
    return of(this.planAdapter.adapt(this.mock_plan));
  }

  getAsync(): Plan {
    return this.planAdapter.adapt(this.mock_plan);
  } 

  getById(id: number): Observable<Plan> {
    return this.http.get<Plan>(this._url+id).pipe(
      map((data: any) => data.results.map(item=>this.planAdapter.adapt(item)))
    );
  }


}
