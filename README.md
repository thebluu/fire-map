# FireMap
Projet frontend en Angular7.


## Requirements : 

Node version 14 (utilisez [NVM](https://github.com/coreybutler/nvm-windows)) pour gérer vos versions Node

## Start : 

Pour démarrer l'appli : 


`npm install`


`npm run start`

## Info générales : 

le projet est ancien et doit être considéré comme obsolète. Les versions de dépendances utilisées datent au mieux de 2018 et ne peuvent plus être considérées comme sécurisées.

Si reprise du projet, il est vivement conseillé de passer sur une version plus récente d'Angular / d'un autre framework.
